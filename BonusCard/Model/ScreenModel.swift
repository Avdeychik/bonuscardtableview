//
//  ScreenModel.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation

protocol ScreenModel: AnyObject {
    
    var cellModels: [CellModel] { get }
}
