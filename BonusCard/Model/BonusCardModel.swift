//
//  BonusCardModel.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation

class BonusCardModel: ScreenModel {
      
    lazy var dataSource = BonusCardDataSource(input: self)
    
    private(set) var cellModels: [CellModel] = [
        
        BonusCardCellModel(
                           textCardSum: "сумма покупок",
                           textCardBalance: "4 300",
                           textCardNumber: "123 321 123"
        ),
    ]
}
