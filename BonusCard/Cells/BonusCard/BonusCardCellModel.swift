//
//  BonusCardCellModel.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation

class BonusCardCellModel: CellModel {
    
    let textCardSum: String
    let textCardBalance : String
    let textCardNumber : String
    
    
    init(textCardSum: String, textCardBalance: String, textCardNumber: String) {

        self.textCardSum = textCardSum
        self.textCardBalance = textCardBalance
        self.textCardNumber = textCardNumber
        
        super.init(cellIdentifier: BonusCardCell.cellIdentifier)
    }
}
