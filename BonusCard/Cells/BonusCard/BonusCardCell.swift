//
//  BonusCardCell.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import UIKit


class BonusCardCell: UITableViewCell {
    
    static let cellIdentifier = "BonusCardCell"
    
    weak var cellModel: BonusCardCellModel?
    
    //MARK: UI
    
    let cardImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "backgroundCard")
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        return image
    }()
    
    let cardView: UIView = {
        let view = UIView()
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 8
        view.layer.shadowOffset = CGSize(width: 0, height: 10)
        return view
    }()
    
    let cardTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        let font: UIFont? = .systemFont(ofSize: 33, weight: .bold)
        let fontSuper: UIFont? = .systemFont(ofSize: 16, weight: .medium)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: "100 letu", attributes: [.font:font!])
        attString.setAttributes([.font:fontSuper!,.baselineOffset:20], range: NSRange(location:3,length:5))
        label.attributedText = attString
        return label
    }()
    
    let cardSumLabel: UILabel = {
        let label = UILabel()
        label.text = "сумма покупок"
        label.textColor = .white
        label.font = .systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    
    let cardBalanceLabel: UILabel = {
        let label = UILabel()
        label.text = "4 300 $"
        label.textColor = .white
        label.font = .systemFont(ofSize: 18, weight: .medium)
        return label
    }()
    
    let cardNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "№ 123 321 123"
        label.textColor = .white
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textAlignment = .right
        return label
    }()
    
    //MARK: init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .white
        selectionStyle = .none
        configure()
        
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    //MARK: configure
    
    func configure() {
        configureCardView()
        configureImage()
        configureCardTypeLabel()
        configureCardSumLabel()
        configureCardBalanceLabel()
        configureCardNumberLabel()
    }

    func configureCardView() {
        contentView.addSubview(cardView)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            cardView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            cardView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
            cardView.heightAnchor.constraint(equalToConstant: 196)
        ])
    }
    
    func configureImage() {
        cardView.addSubview(cardImage)
        cardImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardImage.topAnchor.constraint(equalTo: cardView.topAnchor),
            cardImage.leftAnchor.constraint(equalTo: cardView.leftAnchor),
            cardImage.rightAnchor.constraint(equalTo: cardView.rightAnchor),
            cardImage.bottomAnchor.constraint(equalTo: cardView.bottomAnchor),
        ])
    }
    
    func configureCardTypeLabel() {
        cardView.addSubview(cardTypeLabel)
        cardTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardTypeLabel.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 10),
            cardTypeLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 20),
            cardTypeLabel.rightAnchor.constraint(equalTo: cardView.rightAnchor, constant: -260),
            cardTypeLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func configureCardSumLabel() {
        cardView.addSubview(cardSumLabel)
        cardSumLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardSumLabel.topAnchor.constraint(equalTo: cardTypeLabel.bottomAnchor, constant: 70),
            cardSumLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 20),
            cardSumLabel.rightAnchor.constraint(equalTo: cardView.rightAnchor, constant: -230),
        ])
    }
    
    func configureCardBalanceLabel() {
        cardView.addSubview(cardBalanceLabel)
        cardBalanceLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardBalanceLabel.topAnchor.constraint(equalTo: cardSumLabel.bottomAnchor, constant: 0),
            cardBalanceLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 20),
            cardBalanceLabel.rightAnchor.constraint(equalTo: cardSumLabel.rightAnchor),
//            cardBalanceLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    func configureCardNumberLabel() {
        cardView.addSubview(cardNumberLabel)
        cardNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardNumberLabel.topAnchor.constraint(equalTo: cardBalanceLabel.topAnchor),
            cardNumberLabel.leftAnchor.constraint(equalTo: cardBalanceLabel.rightAnchor, constant: 20),
            cardNumberLabel.rightAnchor.constraint(equalTo: cardView.rightAnchor, constant: -20),
            cardNumberLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
}

//MARK: extension

extension BonusCardCell: FillableCell {
    
    func fill(cellModel: CellModel) {
        guard let cellModel = cellModel as? BonusCardCellModel else { return }
        self.cellModel = cellModel
        
        cardSumLabel.text = cellModel.textCardSum
        cardBalanceLabel.text = "\(cellModel.textCardBalance) ₽"
        cardNumberLabel.text = "№ \(cellModel.textCardNumber)"
    }
}
