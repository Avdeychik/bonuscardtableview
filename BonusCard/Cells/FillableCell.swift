//
//  FillableCell.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation

protocol FillableCell {
    
    func fill(cellModel: CellModel)
}
