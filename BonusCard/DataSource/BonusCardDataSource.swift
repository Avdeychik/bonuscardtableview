//
//  BonusCardDataSource.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation
import UIKit

class BonusCardDataSource: NSObject, DataSource, UITableViewDataSource {
    
    unowned var input: ScreenModel
    
    init(input: ScreenModel) {
        self.input = input
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return input.cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard input.cellModels.count > indexPath.row else { return .init() }
        let cellModel = input.cellModels[indexPath.row]
        tableView.register(typeInString: cellModel.cellIdentifier)
        let cell = tableView.dequeueReusableCell(
            withIdentifier: cellModel.cellIdentifier, for: indexPath
        )
        
        if let fillableCell = cell as? FillableCell {
            fillableCell.fill(cellModel: cellModel)
        }
        
        return cell
    }
}
