//
//  DataSource.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import Foundation

protocol DataSource {
    
    var input: ScreenModel { get }
}
