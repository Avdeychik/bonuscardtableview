//
//  Extension + UIViewController.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import UIKit

func createCustomTitleView(titleImageName: String) -> UIView {
    
    let view = UIView()
    view.frame = CGRect(x: 0, y: 0, width: 160, height: 44)
    
    let imageTitle = UIImageView()
    imageTitle.image = UIImage(named: titleImageName)
    imageTitle.frame = CGRect(x: 0, y: 0, width: 160, height: 44)
    view.addSubview(imageTitle)
    
    return view
}


extension UITableView {
    func register(typeInString: String) {
        guard let type = NSClassFromString("BonusCard.\(typeInString)") else { return }
        self.register(type.self, forCellReuseIdentifier: typeInString)
    }
}
