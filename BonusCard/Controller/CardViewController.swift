//
//  CardViewController.swift
//  BonusCard
//
//  Created by Алексей Авдейчик on 17.11.21.
//

import UIKit

class CardViewController: UIViewController {
    
    let model = BonusCardModel()
    
    //MARK: UI
    
    let topBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.07286616415, green: 0.05830229074, blue: 0.05857665092, alpha: 1)
        return view
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()

    //MARK: init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        setupNavigationViews()
        configure()
        
        tableView.delegate = self
        tableView.dataSource = model.dataSource
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.bounces = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private func setupNavigationViews() {
        
        let customTitleView = createCustomTitleView(titleImageName: "customTitleImage")
        navigationItem.titleView = customTitleView
        
        let listButton = UIBarButtonItem(image: UIImage(systemName: "list.dash"),
                                         style: .done,
                                         target: self,
                                         action: #selector(barButtonTaped))
        let seacrhButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"),
                                           style: .done,
                                           target: self,
                                           action: #selector(barButtonTaped))
        let lockButton = UIBarButtonItem(image: UIImage(systemName: "bag"),
                                           style: .done,
                                           target: self,
                                           action: #selector(barButtonTaped))
        listButton.tintColor = .white
        seacrhButton.tintColor = .white
        lockButton.tintColor = .white
        
        navigationItem.leftBarButtonItem = listButton
        navigationItem.rightBarButtonItems = [lockButton, seacrhButton]
        
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.07286616415, green: 0.05830229074, blue: 0.05857665092, alpha: 1)
    }
    
    @objc func barButtonTaped() {}
    
    //MARK: configure
    
    func configure() {
        configureTopBackgroundView()
    }
    
    func configureTopBackgroundView() {
            view.addSubview(topBackgroundView)
            topBackgroundView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                topBackgroundView.topAnchor.constraint(equalTo: view.topAnchor),
                topBackgroundView.leftAnchor.constraint(equalTo: view.leftAnchor),
                topBackgroundView.rightAnchor.constraint(equalTo: view.rightAnchor),
                topBackgroundView.heightAnchor.constraint(equalToConstant: 47)
            ])
    }
}

extension CardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        250
    }
}
